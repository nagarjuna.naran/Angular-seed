
/* Services */
var services = angular.module('services', []);

services.factory('myService', function ($http, $q) {
    return {
        // GET
        getData: function () {
            var deferred = $q.defer();
            var promise = $http.get('data/dummy.json').success(function (response) {
                deferred.resolve(response);
            });
            // Return the promise to the controller
            return deferred.promise;
        },

        //POST
        postData: function (movie) {
            var deferred = $q.defer();
            $http.post('/api/v1/movies/' + movie)
                .success(function (data) {
                    deferred.resolve({
                        title: data.title,
                        cost: data.price
                    });
                }).error(function (msg, code) {
                    deferred.reject(msg);
                    $log.error(msg, code);
                });
            return deferred.promise;
        }

    }
});